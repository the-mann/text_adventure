from setuptools import setup

setup(
    name='text_adventure',
    version='0.1',
    packages=['text_adventure'],
    url='',
    license='',
    author='Marcus',
    author_email='mitymarcus@gmail.com',
    description='A fun text adventure', install_requires=['prettytable', 'blessings']
)

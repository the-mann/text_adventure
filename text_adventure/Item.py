class Item:
    def __init__(self, name, cost=5, amount=1, description="FILL OUT DESCRIPTION"):
        self.description = description
        self.cost = cost
        self.name = name
        self.amount = amount
    def __str__(self):
        return f"{self.amount}x {self.name}"

class FracturedBone(Item):
    def __init__(self, name="Fractured Bone", cost=15, amount=1, description="A crooked old bone with a large crack in the middle"):
        super().__init__(name, cost, amount, description)
from text_adventure.Item import Item


class Weapon(Item):
    def __init__(self, name, cost: int, damage: int, effect, damage_type):
        super().__init__(name, cost)
        self.effect = effect
        self.damage = damage
        self.amount = 1
        self.damage_type = damage_type

class Rock(Weapon):
    def __init__(self, name="Rock", cost=2, damage=1, effect=None):
        super().__init__(name, cost, damage, effect, damage_type="Sharpness")
        self.description = f"You found this somewhere on the ground... Does {damage} damage."

class RustyDagger(Weapon):
    def __init__(self, name="Rusty Dagger", cost=10, damage=2, effect=None, damage_type="Sharpness"):
        super().__init__(name, cost, damage, effect, damage_type)
        self.description = f"Your faithful rusty dagger... Does {damage} damage."

from random import randint

from text_adventure.Item import FracturedBone


class Interaction:
    def __init__(self, title):
        self.title = title

    def action(self, player):
        pass


class Skeleton(Interaction):
    def __init__(self, title="Skeleton"):
        super().__init__(title)
    def action(self, player):
        player.inventory.give_item(FracturedBone())
        print("You find a fractured bone in the chest of the skeleton... You think it may have been attacked by the moose... However it wasn't has lucky as you were.")

class Chest(Interaction):
    def __init__(self, title="Chest"):
        super().__init__(title)
    def action(self, player):
        amount = randint(2,9)
        print(f"You find {amount} gems in the chest.")
        player.money += amount
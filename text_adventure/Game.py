from text_adventure.Food import Apple
from text_adventure.Inventory import Inventory
from text_adventure.Player import Player
from text_adventure.Weapon import RustyDagger
from text_adventure.Map import Map

class Game:
    def __init__(self):
        self.player = self.generate_player()
        self.game_over = False
    def generate_player(self):
        name = input("What is your player's name?")
        return Player(name, Inventory([Apple(), RustyDagger()]), 2, 4, Map())
    def run_game(self):
        print(f"Hey {self.player.name}, I hope you enjoy my game!")
        print("You wake up in a cave, cold and shivering. You seem to recall a fight you were just in, you thought you remembered a moose or something.")
        while not self.game_over: # Main game loop
            command = input("What do you want to do? (type 'help' for help):")
            self.player.execute_cmd(command)

if __name__ == "__main__":
    Game().run_game()


from text_adventure.Interaction import Skeleton, Chest
from text_adventure.Question import Question


class Room:
    def __init__(self, name, x, y, interactions, description):
        """

        :type interactions: list of Questions
        """
        self.description = description
        self.name = name
        self.x = x
        self.y = y
        self.interactions = interactions

    def __str__(self):
        return str(self.name)


class Cave1(Room):
    def __init__(self, x, y, name="Cave 1", interactions=(Skeleton(), Chest()), description="As you glance around the room, you notice a skeleton lying in the corner. You also see a chest."):
        super().__init__(name, x, y, interactions, description)


class Cave2(Room):
    def __init__(self, x, y, name="Cave 2", interactions=(Skeleton(), Chest()), description="As you glance around the room, you notice a skeleton lying in the corner. You also see a chest."):
        super().__init__(name, x, y, interactions, description)


class Cave3(Room):
    def __init__(self, x, y, name="Cave 3", interactions=(Skeleton(), Chest()), description="As you glance around the room, you notice a skeleton lying in the corner. You also see a chest."):
        super().__init__(name, x, y, interactions, description)


class Cave4(Room):
    def __init__(self, x, y, name="Cave 4", interactions=(Skeleton(), Chest()), description="As you glance around the room, you notice a skeleton lying in the corner. You also see a chest."):
        super().__init__(name, x, y, interactions, description)
